import { combineReducers } from 'redux';
import authReducer from './auth/reducers';
import testReducer from './test/reducers';

const rootReducer = combineReducers({
  auth: authReducer,
  test: testReducer
});

export default rootReducer;
