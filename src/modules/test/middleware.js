import axios from 'axios'

export default class TestMiddleware {

  // Signin Functions Starts
  static loadData() {
    return (dispatch) => {
      dispatch({ type: 'DATA_LOADING' });
      axios({
        method: 'get',
        url: 'https://jsonplaceholder.typicode.com/posts',
        responseType: 'json'
      })
      .then((response) => {
        if (response && response.status === 200 && response.data) {
          dispatch({ type: 'DATA_SUCCESS', data: response.data });
        }
      })
      .catch((error) => {
        console.log(error)
        dispatch({ type: 'DATA_FAILED' });
      });
    };
  }
}
