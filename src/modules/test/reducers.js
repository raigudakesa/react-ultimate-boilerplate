const INITIAL_STATE = {
  data: [],
  loading: false
};

function TestReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
  case 'DATA_SUCCESS':
    return { ...state, loading: false, data: action.data };
  case 'DATA_FAILED':
    return { ...state, loading: false };
  case 'DATA_LOADING':
    return { ...state, loading: true };
  default:
    return { ...state, loading: false };
  }
}

export default TestReducer;
