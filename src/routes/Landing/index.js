import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AuthMiddleware from 'modules/auth/middleware';
import TestMiddleware from 'modules/test/middleware';

const mapStateToProps = (state) => {
  return {
    test: state.test
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    logout: () => AuthMiddleware.logout(),
    loadData: () => TestMiddleware.loadData()
  }, dispatch);
};

export class Landing extends Component {
  static propTypes = {
    logout: PropTypes.func.isRequired,
    loadData: PropTypes.func.isRequired,
    test: PropTypes.object.isRequired
  };

  // constructor(props) {
  //   super(props);
  // }

  componentWillMount() {}
  componentWillUnmount() {}

  _loadData() {
    this.props.loadData();
  }

  render() {
    const { logout } = this.props;
    console.log(this.props.test)
    const table = (
      this.props.test.data && this.props.test.data.length > 0 ? (
        <table style={{ border: '1px solid #eaeaea' }}>
          <thead>
            <tr>
              <th style={{ minWidth: 200 }}>Title</th>
              <th>Body</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.test.data && this.props.test.data.map((o, i) => {
                return (
                  <tr key={['table_', i]}>
                    <td>{o.title}</td>
                    <td>{o.body}</td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      ) : (
        <div>Data Not Found</div>
      )
    )
    return (
      <section className="container text-xs-center">
        <h4>Hello Landing Page</h4>
        <a>now the login/signup routes are denied access</a>
        <h4 >
          <button onClick={logout}>Logout</button>&nbsp;
          <button onClick={this._loadData.bind(this)}>Load API</button>
        </h4>
        {
          this.props.test.loading ? (
            <div>Please Wait...</div>
          ) : (
            table
          )
        }
      </section>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
